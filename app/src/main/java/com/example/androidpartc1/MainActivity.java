package com.example.androidpartc1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.textName)
    EditText name;
    @BindView(R.id.day)
    EditText day;
    @BindView(R.id.month)
    EditText month;
    @BindView(R.id.year)
    EditText year;

    @BindView(R.id.buttonContinue)
    Button buttonContinue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        buttonContinue.setEnabled(false);
    }

    @OnClick(R.id.buttonContinue)
    public void onButtonClick() {
        Intent intent = new Intent(this, Statistics.class);
        intent.putExtra(Intent.ACTION_DATE_CHANGED, day.getText().toString());
        intent.putExtra(Intent.ACTION_DATE_CHANGED, month.getText().toString());
        intent.putExtra(Intent.ACTION_DATE_CHANGED, year.getText().toString());
        intent.putExtra(Intent.EXTRA_COMPONENT_NAME, name.getText().toString());
        startActivity(intent);
        buttonContinue.setEnabled(true);
    }
}
